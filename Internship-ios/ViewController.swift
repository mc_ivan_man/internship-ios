//
//  ViewController.swift
//  Internship-ios
//
//  Created by Ivan on 15.06.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var nextTitleText: String? = "Заголовок"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBAction func touchNextButton(_ sender: UIButton) {
        
        guard let viewController = storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController else {
            return
        }
        if textField.text != ""{
            viewController.nextTitleText = textField.text
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.title = nextTitleText
        
        let numberOfViewControler = navigationController?.viewControllers.count ?? 7
        titleLabel.text = "Экран №\(numberOfViewControler)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

